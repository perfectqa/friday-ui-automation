package Utilities;
import org.testng.annotations.DataProvider;

public class DataProviderFactory{

    @DataProvider(name = "firstCarData")
    public static Object[][] firstCarDataSet(){
        return new Object[][]{
                {"MERCEDES-BENZ", "SLC", "01.02.2021","Benzin","270 kW / 367 PS", "05.2015", "12.2020"},
                {"MERCEDES-BENZ","AMG GT","01.02.2021","Diesel", "390 kW / 530 PS","04.2017", "10.2020"},
                {"MERCEDES-BENZ","A-Klasse", "01.02.2021","Benzin","310 kW / 421 PS", "05.2015", "11.2020"}};
    }
    @DataProvider(name = "secondCarData")
    public static Object[][] secondCarDataSet(){
        return new Object[][]{
                {"BMW","i8","01.02.2021","Benzin","287 kW / 390 PS","05.2015","11.2020"},
                {"BMW","X3","01.01.2021","Hybrid", "390 kW / 530 PS","06.2019", "12.2020"},
                {"BMW","Z8","01.12.2020","Hybrid", "390 kW / 530 PS","01.2019", "12.2020"}};}

    @DataProvider(name = "thirdCarData")
    public static Object[][] thirdCarDataSet(){
        return new Object[][]{
                {"HYUNDAI","Accent","01.02.2021","Diesel", "81 kW / 110 PS","05.2015", "11.2020"},
                {"HYUNDAI","Sonata","01.05.2021","Benzin","184 kW / 250 PS","03.2011","12.2020"},
                {"HYUNDAI","Genesis","01.01.2021","Benzin","223 kW / 303 PS","08.2018","11.2019"}
        };
    }
}