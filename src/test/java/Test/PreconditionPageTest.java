package Test;
import Pages.CarRegisterationPages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class PreconditionPageTest{
    private WebDriver driver;
    private String url = "https://hello.friday.de/quote/selectModel";

    public PreconditionPageTest(WebDriver driver) {
        this.driver = driver;
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public void preconditionPage(String insuredDate, String type) throws InterruptedException {
        PreconditionPageFactory preconditionPageFactory = new PreconditionPageFactory(driver);
        if(type.equals("insured")){
            preconditionPageFactory.preparationForInsuredCars(insuredDate);}
        else if(type.equals("new")){
            preconditionPageFactory.preparationForNewCars(insuredDate);
        }
    }
    public void registeredOwnerPage(String type){
        RegisteredOwnerPageFactory registeredOwnerPageFactory = new RegisteredOwnerPageFactory(driver);
        if(type.equals("insured")){
            registeredOwnerPageFactory.selectOwnerPageDefaultOption();}
        else if(type.equals("new")){
            registeredOwnerPageFactory.selectNewOwnerNewCarOption();
        }
        else if(type.equals("semi-new")){
            registeredOwnerPageFactory.selectNewOwnerInsuredCarOption();
        }
    }
    public void vehiclePage(String vehicleBrand) {
        VehiclePageFactory vehiclePageFactory = new VehiclePageFactory(driver);
        vehiclePageFactory.searchVehicle(vehicleBrand);
    }
    public void modelPage(String vehicleType){
        ModelPageFactory modelPageFactory = new ModelPageFactory(driver);
        modelPageFactory.searchVehicleModel(vehicleType);
    }
    public void fuelTypePage(String fuelType){
        FuelTypePageFactory fuelTypePageFactory = new FuelTypePageFactory(driver);
        fuelTypePageFactory.selectFuelType(fuelType);
    }
    public void enginePowerPage(String enginePower){
        EnginePowerPageFactory enginePowerPageFactory = new EnginePowerPageFactory(driver);
        enginePowerPageFactory.selectEnginePower(enginePower);
    }
    public void enginePage(){
        EnginePageFactory enginePageFactory = new EnginePageFactory(driver);
        enginePageFactory.selectEngine();
    }
    public void registrationDate(String fDate, String lDate){
        RegistrationDateFactory registrationDateFactory = new RegistrationDateFactory(driver);
        //registrationDateFactory.VehicleRegistrationGuid();
        registrationDateFactory.enterRegistrationDate(fDate, lDate);
    }
    public Boolean isBirthDatePage(){
        Boolean result;
        BirthDatePageFactory birthDatePageFactory = new BirthDatePageFactory(driver);
        result = birthDatePageFactory.isBirthDayPage();
        return result;
    }
    public void backBtn(String url){
        BirthDatePageFactory birthDatePageFactory = new BirthDatePageFactory(driver);
        birthDatePageFactory.selectBackBtn(url);
    }

    public void preparationForDefaultCar(String insuredDate) throws InterruptedException {

        preconditionPage(insuredDate,"insured");
        registeredOwnerPage("insured");

    }

    public void preparationForNewOwnerNewCar(String insuredDate) throws InterruptedException {
        preconditionPage(insuredDate,"new");
        registeredOwnerPage("new");
    }

    public void preparationForNewOwnerInsuredCar(String insuredDat) throws InterruptedException {
        preconditionPage(insuredDat, "insured");
        registeredOwnerPage("semi-new");

    }
    public void vehicleInfRegistration(String vehicleBrand, String vehicleType,
                                       String fuelType, String enginePower, String fDate, String lDate) throws InterruptedException {
        vehiclePage(vehicleBrand);
        modelPage(vehicleType);
        Thread.sleep(2000);
        fuelTypePage(fuelType);
        Thread.sleep(2000);
        enginePowerPage(enginePower);
        enginePage();
        registrationDate(fDate,lDate);
        Assert.assertTrue(isBirthDatePage());
        backBtn(url);
    }
//    @Test(dataProvider = "firstCarData", priority = 1)
//    public void firstCarTest(String vehicleBrand, String vehicleType, String insuredDate,
//                             String fuelType, String enginePower, String fDate, String lDate ) throws InterruptedException {
//        registrationWithDefaultInfoTest(vehicleBrand,vehicleType,insuredDate,fuelType,enginePower,fDate,lDate);
//
//    }
//    @Test(dataProvider = "secondCarData", priority = 2)
//    public void secondCarTest(String vehicleBrand, String vehicleType, String insuredDate,
//                             String fuelType, String enginePower, String fDate, String lDate ) throws InterruptedException {
//        registrationWithDefaultInfoTest(vehicleBrand,vehicleType,insuredDate,fuelType,enginePower,fDate,lDate);
//
//    }
//    @Test(dataProvider = "thirdCarData", priority = 3)
//    public void thirdCarTest(String vehicleBrand, String vehicleType, String insuredDate,
//                             String fuelType, String enginePower, String fDate, String lDate ) throws InterruptedException {
//        registrationWithDefaultInfoTest(vehicleBrand,vehicleType,insuredDate,fuelType,enginePower,fDate,lDate);
//
//    }


}
