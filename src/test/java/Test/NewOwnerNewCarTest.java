package Test;

import Base.DriverManager;
import Base.DriverManagerFactory;
import Utilities.DataProviderFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class NewOwnerNewCarTest {

    WebDriver driver;
    DriverManager driverManager;

    @Parameters("browserType")
    @BeforeTest
    public void setup(String browser) {
        driverManager = DriverManagerFactory.getManager(browser);
        driver = driverManager.getDriver();
    }

    @AfterClass
    public void quitDriver() {
        driverManager.quitDriver();
    }

    @Test(dataProvider = "secondCarData", dataProviderClass = DataProviderFactory.class)
   public void secondCarTest(String vehicleBrand, String vehicleType, String insuredDate,
                            String fuelType, String enginePower, String fDate, String lDate ) throws InterruptedException {
        PreconditionPageTest preconditionPageTest = new PreconditionPageTest(driver);
        preconditionPageTest.preparationForNewOwnerNewCar(insuredDate);
        preconditionPageTest.vehicleInfRegistration(vehicleBrand,vehicleType,fuelType,enginePower,fDate,lDate);
   }
}
