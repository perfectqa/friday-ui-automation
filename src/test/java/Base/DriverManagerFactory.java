package Base;

import io.github.bonigarcia.wdm.managers.FirefoxDriverManager;

public class DriverManagerFactory {

    public static DriverManager getManager(String  type) {

        DriverManager driverManager;

        switch (type) {
            case "chrome":
            default:
                driverManager = new ChromeDriverManager();
                break;
            case "firefox":
                driverManager = new FireFoxDriverManager();
                break;
        }
        return driverManager;

    }
}