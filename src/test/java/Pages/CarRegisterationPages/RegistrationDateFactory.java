package Pages.CarRegisterationPages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

public class RegistrationDateFactory {
    WebDriver driver;

    @FindBy(name = "monthYearFirstRegistered")
    WebElement FirstRegistrationDate;

    @FindBy(name = "monthYearOwnerRegistered")
    WebElement LastRegistrationByOwner;

    @FindBy(xpath = "//button[@type='submit']")
    WebElement SubmitBtn;

    @FindBy(xpath = "//*[text()='Fahrzeugschein']")
    WebElement VehicleRegistrationDocument;

    @FindBy(xpath = "//section/div[1]/a")
    WebElement ModalCloseBtn;

    @FindBy(xpath = "//section/div[1]/div/span")
    WebElement RegistrationDocHeader;


    public RegistrationDateFactory(WebDriver driver){
        this.driver = driver;
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public void inputFirstRegistrationDate(String date){
        if (FirstRegistrationDate.isDisplayed()){
             FirstRegistrationDate.sendKeys(date);
        }
    }
    public void inputLastRegistrationByOwner(String date){
        try{
            if(LastRegistrationByOwner.isDisplayed()){
                LastRegistrationByOwner.sendKeys(date);}}
        catch (NoSuchElementException ex){
            Reporter.log("Not found the element", true);
        }
    }
    public void clickSubmitBtn(){
        SubmitBtn.click();
    }
    public void clickVehicleRegistrationDoc(){
        VehicleRegistrationDocument.click();
    }
    public Boolean checkVehicleRegistrationDocModal(){
        Boolean result = false;
        try {
            if (RegistrationDocHeader.isDisplayed()){
                result = true;
            }
        }catch (NoSuchElementException ex){
            Reporter.log("Not found the element", true);
        }
        return result;
    }
    public void closeModal(){
        ModalCloseBtn.click();
    }
    public void  VehicleRegistrationGuid(){
        Boolean header;
        clickVehicleRegistrationDoc();
        header = checkVehicleRegistrationDocModal();
        Assert.assertTrue(header);
        closeModal();
    }
    public void enterRegistrationDate(String fDate, String lDate){
        inputFirstRegistrationDate(fDate);
        inputLastRegistrationByOwner(lDate);
        clickSubmitBtn();
    }
}
