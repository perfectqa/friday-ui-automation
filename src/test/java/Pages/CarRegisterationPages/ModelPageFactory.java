package Pages.CarRegisterationPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ModelPageFactory {
    private WebDriver driver;

    @FindBy(name = "modelFilter")
    WebElement SearchBox;


    public ModelPageFactory(WebDriver driver){
        this.driver=driver;
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public WebElement selectItemFromList(String searchItem){
       WebElement element;
       element = driver.findElement(By.xpath("//*[text()='Model X']"));
       return element;
    }
    public WebElement selectSearchItem(String searchItem){
        WebElement element;
        element = driver.findElement(By.xpath("//*[starts-with(text(),'"+searchItem+"')]"));
        return element;
    }
    public void searchVehicleModel(String searchItem){
        //search model by search box
        if(SearchBox.isDisplayed())
        {
            SearchBox.clear();
            SearchBox.sendKeys(searchItem);
            selectSearchItem(searchItem).click();
        }else{
            //search model by selecting from list
            selectItemFromList(searchItem);
        }
    }
}
