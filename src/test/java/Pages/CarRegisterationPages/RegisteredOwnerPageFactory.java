package Pages.CarRegisterationPages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;

public class RegisteredOwnerPageFactory {
    private WebDriver driver;
    String ownerDefaultPageUrl="https://hello.friday.de/quote/selectRegisteredOwner";

    @FindBy(xpath = "//*[@value='Yes']")
    WebElement IsCarOwner;

    @FindBy(xpath = "//*[@value='No']")
    WebElement IsNotCarOwner;

    @FindBy(xpath = "//*[@value='used']")
    WebElement IsCarUsed;

    @FindBy(xpath = "//*[@value='brandNew']")
    WebElement IsCarNew;

    @FindBy(xpath = "//button[@type='submit']")
    WebElement SubmitBtn;

    public RegisteredOwnerPageFactory(WebDriver driver){
        this.driver=driver;

        //Initialise Elements
        PageFactory.initElements(driver, this);
    }

    public Boolean checkDefaultCarOwner(){
        if (!IsCarOwner.isSelected()){
            IsCarOwner.click();
        }
        return true;
    }
    public void clickNotCarOwner(){
        IsNotCarOwner.click();
    }
    public void clickUsedCar(){
        if (!IsCarUsed.isSelected()){
            IsCarUsed.click();
        }
    }
    public void clickIsCarNew(){
        IsCarNew.click();
    }
    public void clickSubmitBtn(){
        SubmitBtn.click();
    }

    public void selectOwnerPageDefaultOption(){
        try{
        //check the driver is in owner default page
        if(driver.getCurrentUrl().equals(ownerDefaultPageUrl)){
            checkDefaultCarOwner();
            clickUsedCar();
            clickSubmitBtn();}
        } catch (NoSuchElementException ex){
            Reporter.log("Is not in Owner page", true);
        }
    }
    public void selectNewOwnerNewCarOption(){
        try{
        if(driver.getCurrentUrl().equals(ownerDefaultPageUrl)){
            clickNotCarOwner();
            clickIsCarNew();
            clickSubmitBtn();}
        } catch (NoSuchElementException ex){
            Reporter.log("Is not in Owner page", true);
        }
    }
    public void selectNewOwnerInsuredCarOption(){
        try{
            if(driver.getCurrentUrl().equals(ownerDefaultPageUrl)){
                clickNotCarOwner();
                clickUsedCar();
                clickSubmitBtn();}
        } catch (NoSuchElementException ex){
            Reporter.log("Is not in Owner page", true);
        }
    }
    public void selectDefaultOwnerNewCarOption(){
        checkDefaultCarOwner();
        clickIsCarNew();
        clickSubmitBtn();
    }

}
