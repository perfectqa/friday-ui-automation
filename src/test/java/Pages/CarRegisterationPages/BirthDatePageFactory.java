package Pages.CarRegisterationPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BirthDatePageFactory {
    WebDriver driver;
    String pageUrl = "https://hello.friday.de/quote/enterBirthDate";

    @FindBy(xpath = "//button[@data-test-id='wizardBackButton']")
    WebElement BackBtn;

    public BirthDatePageFactory(WebDriver driver){
        this.driver = driver;
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public Boolean isBirthDayPage(){

        if(driver.getCurrentUrl().
                equalsIgnoreCase(pageUrl)){
            return true;
        }
        else
            return false;
    }
    public void selectBackBtn(String url){
        if (BackBtn.isDisplayed()){
            while (!driver.getCurrentUrl().equalsIgnoreCase(url)){
                try{
                BackBtn.click();}
                catch (org.openqa.selenium.StaleElementReferenceException  ex) {
                    BackBtn.click();
                }
                }
            }
        }
    }


