package Pages.CarRegisterationPages;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;


public class PreconditionPageFactory {
    private WebDriver driver;
    String pageUrl = "https://hello.friday.de/quote/selectPrecondition";

    @FindBy(xpath = "//*[@value='buyingCar']")
    WebElement StillRegistering;

    @FindBy(xpath = "//*[@value='keepingCar']")
    WebElement AlreadyRegistered;

    @FindBy(name = "inceptionDate")
    WebElement InsuranceStartDate;

    @FindBy(xpath = "//button[@type='submit']")
    WebElement SubmitBtn;

    @FindBy(id = "uc-btn-accept-banner")
    WebElement AcceptCookieBtn;

    public PreconditionPageFactory(WebDriver driver) {
        this.driver=driver;

        //Initialise Elements
        PageFactory.initElements(driver, this);
    }


    public void clickAcceptCookieBtn() throws InterruptedException {
        try{
            if(AcceptCookieBtn.isDisplayed()){
            AcceptCookieBtn.click();
        }}
        catch (NoSuchElementException ex){
            Reporter.log("The element is invisible", true);
        }
    }

    public void clickStillRegistering(){
        StillRegistering.click();
    }

    public void clickAlreadyInsuredCars(){
        AlreadyRegistered.click();
    }

    public void setInsuranceStartDate(String date){
        InsuranceStartDate.sendKeys(date);
    }

    public void clickSubmitButton(){
        SubmitBtn.click();
    }

    public void preparationForInsuredCars(String date) throws InterruptedException {
        //check if the driver is in home page
        if(driver.getCurrentUrl().equals(pageUrl)){
            clickAcceptCookieBtn();
            clickAlreadyInsuredCars();
            setInsuranceStartDate(date);
            clickSubmitButton();
        }
    }

    public void preparationForNewCars(String date) throws InterruptedException {
        //check if the driver is in home page
        if(driver.getCurrentUrl().equals(pageUrl)){
            clickAcceptCookieBtn();
            clickStillRegistering();
            setInsuranceStartDate(date);
            clickSubmitButton();
        }
    }


}
