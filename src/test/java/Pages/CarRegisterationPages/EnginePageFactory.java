package Pages.CarRegisterationPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnginePageFactory {
    WebDriver driver;

    @FindBy(name = "engine")
    WebElement selectEngine;

    @FindBy(xpath = "//*[starts-with(text(),'HSN / TSN')]")
    WebElement inputVehicleNum;


    public EnginePageFactory(WebDriver driver){
        this.driver=driver;
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }

    public void clickEngine(){
        selectEngine.click();
    }
    public void selectEngine(){
        clickEngine();
    }
}
