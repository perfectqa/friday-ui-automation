package Pages.CarRegisterationPages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;

public class FuelTypePageFactory {
    WebDriver driver;

    public FuelTypePageFactory(WebDriver drive){
        this.driver= drive;

        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public WebElement defineFuelType(String fuelType){
        WebElement element;
        element = driver.findElement(By.xpath("//*[starts-with(text(),'"+fuelType+"')]"));
        return element;
    }
    public void selectFuelType(String fuelType){
        String pageUrl = "https://hello.friday.de/quote/selectFuelType";
        try{
        if (driver.getCurrentUrl().equals(pageUrl)){
            defineFuelType(fuelType).click();
        }}
        catch (NoSuchElementException ex){
            Reporter.log("No fuel page ", true);
        }
    }
}
