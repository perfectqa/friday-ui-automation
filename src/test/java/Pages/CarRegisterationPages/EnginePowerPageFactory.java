package Pages.CarRegisterationPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnginePowerPageFactory {
    WebDriver driver;



    public EnginePowerPageFactory(WebDriver driver){
        this.driver = driver;
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public WebElement defineEnginePower(String enginePower){
        WebElement element;
        element = driver.findElement(By.xpath("//*[starts-with(text(),'"+enginePower+"')]"));
        return element;
    }

    public void selectEnginePower(String enginePower){
        String pageUrl = "https://hello.friday.de/quote/selectEnginePower";
        if (driver.getCurrentUrl().equalsIgnoreCase(pageUrl)){
            defineEnginePower(enginePower).click();
        }
    }
}
