package Pages.CarRegisterationPages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class VehiclePageFactory {
    private WebDriver driver;
    private String pageUrl = "https://hello.friday.de/quote/selectVehicle";

    @FindBy(name = "makeFilter")
    WebElement SearchBox;

    public VehiclePageFactory(WebDriver driver){
        this.driver=driver;

        //Initialise Elements
        PageFactory.initElements(driver, this);
    }
    public WebElement selectSearchItem(String searchItem){
        WebElement element;
        element = driver.findElement(By.xpath("//*[starts-with(text(),'"+searchItem+"')]"));
        return element;
    }
    public void searchVehicle(String searchItem) {
        if(driver.getCurrentUrl().equals(pageUrl)){
            SearchBox.clear();
            SearchBox.sendKeys(searchItem);
            selectSearchItem(searchItem).click();
        }
    }

}
